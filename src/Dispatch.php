<?php

namespace ARIA\Webhooks;

use GuzzleHttp\Client;

class Dispatch
{

    /**
     * Dispatch a webhook event to an endpoint.
     *
     * @param string $endpoint
     * @param Webhook $payload
     * @return array|null
     */
    public static function dispatch(string $endpoint, Webhook $payload): ? array
    {
        $endpoint = trim($endpoint);

        // Validate url
        if (!filter_var($endpoint, FILTER_VALIDATE_URL)) {
            throw new WebhookException('Endpoint value is not a URL');
        }

        // Dispatch webhook to endpoint
        $client = new Client();

        $headers = [
            'Content-Type' => 'application/json',
            'Accept' => 'application/json'
        ];
        
        $response = $client->request('POST', $endpoint, [
            'headers' => $headers,
            'body' => json_encode($payload)
        ]);

        $status = $response->getStatusCode();
        if ($status!=200) throw new WebhookException("Webhook dispatch to $endpoint returned status $status", $status);

        $responsebody = json_decode($response->getBody(), true);

        if (!empty($responsebody)) {
            
            return $responsebody;

        } else {
            throw new WebhookException('Webhook response was not valid JSON');
        }

        return null;
    }
}
