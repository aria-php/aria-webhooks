<?php

namespace ARIA\Webhooks;

use RuntimeException;

class WebhookException extends RuntimeException {}