<?php

namespace ARIA\Webhooks;

/**
 * Webhook payload class
 */
class Webhook implements \JsonSerializable
{
    /**
     * Event being triggered
     *
     * @var string
     */
    private string $event = "";

    /**
     * Context related to the webhook
     *
     * @var array
     */
    private array $context = [];

    /**
     * The payload
     *
     * @var array
     */
    private array $payload = [];

    /**
     * Create a webhook ready for dispatch
     *
     * @param string $event
     * @param array $payload
     * @param array $context
     */
    public function __construct(string $event, array $payload = [], array $context = [])
    {
        $this->event = $event;
        $this->payload = $payload;
        $this->context = $context;

        // TODO: Construct signature
    }

    /**
     * Validate an incoming webhook
     *
     * @param array $webhook
     * @throws WebhookException
     * @return boolean
     */
    protected static function validateIncoming(array $webhook) : bool 
    {
        if (empty($webhook['version'])) throw new WebhookException('Webhook is missing version information');
        if (!in_array($webhook['version'], ['1.0'])) throw new WebhookException('Unsupported webhook version');

        if (empty($webhook['event'])) throw new WebhookException('Event filed must be present in webhook');
        if (empty($webhook['payload'])) throw new WebhookException('Payload filed must be present in webhook');


        // TODO: Validate a signature


        return true;
    }

    /**
     * Construct a webhook object out of a json blob.
     * 
     * Take an incoming webhook blob and convert it into a webhook object.
     *
     * @param array $webhook
     * @return Webhook
     */
    public static function factory(array $webhook) : Webhook {

        if (!static::validateIncoming($webhook)) {
            throw new WebhookException('Webhook payload is not valid');
        }

        $obj = new Webhook($webhook['event'], $webhook['payload'], $webhook['context']);

        return $obj;
    }

    public function jsonSerialize(): mixed
    {
        return [
            'version' => 1.0,
            'event' => $this->event,
            'context' => $this->context,
            'payload' => $this->payload
        ];
    }
}
