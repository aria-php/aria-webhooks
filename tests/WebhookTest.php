<?php

use ARIA\Webhooks\Dispatch;
use ARIA\Webhooks\Webhook;

class WebhookTest extends \PHPUnit\Framework\TestCase {

    public function webhookProvider() {
        return [

            "Webhook" => [
                new Webhook('webhook.test', [
                    'p1' => 'a', 'p2' => 'b'
                ], ['context' => 'test'])
            ]

        ];
    }
    
    /**
     * Test the connection
     *
     * @dataProvider webhookProvider
     * @param Webhook $webhook
     * @return void
     */
    public function testConnection(Webhook $webhook) {

        $response = Dispatch::dispatch($_ENV['ENDPOINT'], $webhook);

        $this->assertTrue(is_array($response));

    }

}