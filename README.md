# ARIA Webhooks framework

This is a library to produce a standard webhook payload format. This will be used throughout the ARIA platform, and so needs to be consistent.

## Installation

`composer require aria-php/aria-webhooks`

## Usage 

